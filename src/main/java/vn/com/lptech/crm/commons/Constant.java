package vn.com.lptech.crm.commons;

/**
 * @author: hai
 * @date: 24/09/2021
 */
public class Constant {
  public static final String CUSTOMER_CODE = "customerCode";
  public static final String CUSTOMER_NAME = "customerName";
  public static final String PHONES = "phones";
  public static final String SECTOR = "sector";
  public static final String ACCOUNT_OFFICE = "accountOffice";
  public static final String TRANSACTION_FREQUENCY = "transactionFrequency";
  public static final String BRANCH_CODE = "branchCode";
  public static final String STATUS = "status";
  public static final String BUSINESS_REGISTRATION_NUMBER = "businessRegistrationNumber";
  public static final String TAX_CODE = "taxCode";
  public static final String ID_CARD = "idCard";
  public static final String URL_RM =
      "http://localhost:10175/crm01-trainning/dummy/ms-core/rm/validate";
  public static final String RM_CODE = "rmCode";
  public static final String HRIS_CODE = "hrisCode";
  public static final String FULL_NAME = "fullName";
  public static final String DIVISION_CODE = "divisionCode";
  public static final String TELEPHONE = "telephone";
  public static final String EMAIL = "email";


  public static final String GROUP_CODE = "groupCode";
  public static final String EMPLOYEE_TYPE = "employeeType";
  public static final String GROUP_NAME = "groupName";
  public static final String CATEGORY_COMMON_CODE = "CrmTrainning";
  public static final String GROUP_SIZE = "groupSize";


}
