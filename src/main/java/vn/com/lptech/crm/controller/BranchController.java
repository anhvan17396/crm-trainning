package vn.com.lptech.crm.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.com.lptech.crm.services.BranchService;

@Slf4j
@RequestMapping("/branches")
@RestController
public class BranchController {

  private final BranchService service;

  public BranchController(BranchService service) {
    this.service = service;
  }


  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> findAll(){
    return new ResponseEntity<>(service.findAll(),HttpStatus.OK);
  }
}
