package vn.com.lptech.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "vn.com.lptech.crm.repositories")
public class CrmServiceApplication {
  public static void main(String[] args) {
    SpringApplication.run(CrmServiceApplication.class);
  }
}
