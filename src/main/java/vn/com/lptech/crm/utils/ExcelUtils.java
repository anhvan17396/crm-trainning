package vn.com.lptech.crm.utils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.CollectionUtils;
import vn.com.lptech.crm.configurations.annotation.ExportExcel;
import vn.com.lptech.crm.exceptions.CrmException;

@Slf4j
public class ExcelUtils {

  /**
   * Method dùng chung cho việc xuất excel
   *
   * @param list
   * @return XSSWorkbook
   * @throws Exception
   */
  public static XSSFWorkbook executeExport(List<Object> list) throws Exception {
    if (CollectionUtils.isEmpty(list)) {
      log.info("...");
      throw new CrmException("...");
    }
    List<String> headers = new ArrayList<>();
    List<String> exportFields = new ArrayList<>();
    Field[] allFields = list.get(0).getClass().getDeclaredFields();
    for (Field f : allFields) {
      ExportExcel colHeader = f.getAnnotation(ExportExcel.class);
      if (colHeader != null) {
        headers.add(colHeader.title());
        exportFields.add(f.getName());
      }
    }
    XSSFWorkbook workbook = new XSSFWorkbook();
    XSSFSheet sheet = workbook.createSheet("Export");
    // Add header template
    Row rowHeader = sheet.createRow(0);
    for (int i = 0; i < headers.size(); i++) {
      Cell cell = rowHeader.createCell(i);
      cell.setCellValue(headers.get(i));
    }
    int rowCount = 1;
    String field = null;
    // Add data
    for (Object dto : list) {
      Row rowData = sheet.createRow(rowCount);
      for (int i = 0; i < exportFields.size(); i++) {
        field = exportFields.get(i);
        Cell cell = rowData.createCell(i);
        cell.setCellValue(String.valueOf(callGetter(dto, field)));
      }
      rowCount++;
    }
    return workbook;
  }

  private static Object callGetter(Object obj, String fieldName) throws Exception {
    PropertyDescriptor pd;
    try {
      pd = new PropertyDescriptor(fieldName, obj.getClass());
      log.info("Field [{}] - Value[{}]", fieldName, pd.getReadMethod().invoke(obj));
      return pd.getReadMethod().invoke(obj);
    } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      throw new CrmException("..");
    }
  }
}
