package vn.com.lptech.crm.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utils {
  private Utils() {
    throw new IllegalStateException("Utils class");
  }

  public static String appendLikeExpression(String value) {
    return String.format("%%%s%%", value);
  }
}
