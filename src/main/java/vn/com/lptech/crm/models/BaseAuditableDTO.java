package vn.com.lptech.crm.models;

import lombok.Data;

import java.time.LocalDate;

@Data
public class BaseAuditableDTO {

  private String createdBy;

  private LocalDate createdDate;

  private String updatedBy;

  private LocalDate updatedDate;
}
