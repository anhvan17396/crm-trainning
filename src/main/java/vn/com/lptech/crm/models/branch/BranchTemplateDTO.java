package vn.com.lptech.crm.models.branch;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class BranchTemplateDTO {
  private String id;
  private long row;
  private String branchCode;
  private String errorType;
  private String errorDescription;
  private String errorData;
  private String requestId;
  private Boolean status;

  public BranchTemplateDTO(String id, int row, String branchCode, String requestId, Boolean status) {
    this.id = id;
    this.row = row;
    this.branchCode = branchCode;
    this.requestId = requestId;
    this.status = status;
  }
}
