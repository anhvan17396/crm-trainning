package vn.com.lptech.crm.repositories;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import vn.com.lptech.crm.domains.Branch;

import java.util.List;
import java.util.Optional;

public interface BranchRepository
    extends PagingAndSortingRepository<Branch, String>, JpaSpecificationExecutor<Branch> {

  Optional<Branch> findTopByCodeOrderByCode(String code);

  List<Branch> findAllByOrderByCode();








  boolean existsBranchByCode(String code);

  Optional<Branch> findByCode(String code);

  List<Branch> findAllByCodeIn(List<String> codes);

  /**
   * @author: ThaoNT
   * @date: 23/09/2021
   * @note: tìm kiếm Chi nhánh theo mã chi nhánh
   */
  Optional<Branch> findTopByCode(String code);

  /**
   * Lấy danh sách chi nhanh theo code
   * @author: khanhnm
   * @param codes danh sách code
   * @return Danh sách chi nhánh
   */
  List<Branch> findBranchesByCodeIn(List<String> codes);
}
