package vn.com.lptech.crm.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import vn.com.lptech.crm.domains.Branch;
import vn.com.lptech.crm.exceptions.CrmException;
import vn.com.lptech.crm.exceptions.ExceptionUtils;
import vn.com.lptech.crm.models.branch.BranchCreateDTO;
import vn.com.lptech.crm.models.branch.BranchDTO;
import vn.com.lptech.crm.models.branch.BranchSearchCriterionDTO;
import vn.com.lptech.crm.repositories.BranchRepository;
import vn.com.lptech.crm.services.BranchService;
import vn.com.lptech.crm.utils.Utils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author: lptech
 * @date: 27/08/2021
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class BranchServiceImpl implements BranchService {


    private final BranchRepository repository;

    /**
     * @author: lptech
     * @date: 27/08/2021
     * @note: Tìm kiếm thông tin chi nhánh theo mã
     */
    @Override
    public BranchDTO findByCode(String code) {
        Optional<Branch> branchOptional = repository.findTopByCodeOrderByCode(code);
        if (branchOptional.isEmpty()) {
            throw new CrmException(
                    String.format(
                            ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE), code));
        }
        Branch branch = branchOptional.get();
        return new BranchDTO(branch);
    }

    /**
     * @author: lptech
     * @date: 27/08/2021
     * @note: Lấy toàn bộ danh sách thông tin chi nhánh Order By theo Code
     */
    @Override
    public List<BranchDTO> findAll()  {
        List<BranchDTO> branchReturns = new ArrayList<>();
        List<Branch> branches = repository.findAllByOrderByCode();
        if (CollectionUtils.isEmpty(branches)) {
            return branchReturns;
        }
        for (Branch branch : branches) {
            branchReturns.add(new BranchDTO(branch));
        }
        return branchReturns;
    }

    /**
     * @author: lptech
     * @date: 27/08/2021
     * @note: Thêm mới chi nhánh
     */
    @Override
    public void create(BranchCreateDTO dto)  {
        if (repository.existsBranchByCode(dto.getCode())) {
            throw new CrmException(
                    String.format(
                            ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_DUPLICATE_CODE), dto.getCode()));
        }
        Branch branch = new Branch();
        branch.setCode(dto.getCode());
        branch.setName(dto.getName());
        repository.save(branch);
        log.info("Object Save: {}", branch);
    }

    /**
     * @author: lptech
     * @date: 27/08/2021
     * @note: Xóa chi nhánh
     */
    @Override
    public void delete(String code){
        Optional<Branch> branchOptional = repository.findTopByCodeOrderByCode(code);
        if (branchOptional.isEmpty()) {
            throw new CrmException(
                    String.format(
                            ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE), code));
        }
        repository.delete(branchOptional.get());
    }

    /**
     * @author: lptech
     * @date: 27/08/2021
     * @note: Lấy toàn bộ danh sách thông tin chi nhánh Order By theo Code
     */
    @Override
    public Page<BranchDTO> findByCriteria(BranchSearchCriterionDTO criterionDTO, Pageable pageable) {
        // 17/09/21 lptech: them sap xep order theo code
        pageable =
                PageRequest.of(
                        pageable.getPageNumber(), pageable.getPageSize(), Sort.by("code").ascending());
        List<BranchDTO> branchReturns = new ArrayList<>();
        // 17/09/21 lptech: buid câu truy vấn sql
        Specification<Branch> specification =
                (root, criteriaQuery, criteriaBuilder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    if (criterionDTO != null) {
                        if (criterionDTO.getName() != null) {
                            predicates.add(
                                    criteriaBuilder.like(
                                            root.get("name"), Utils.appendLikeExpression(criterionDTO.getName())));
                        }
                        if (criterionDTO.getCode() != null) {
                            predicates.add(
                                    criteriaBuilder.like(
                                            root.get("code"), Utils.appendLikeExpression(criterionDTO.getCode())));
                        }
                    }
                    return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
                };
        Page<Branch> branches = repository.findAll(specification, pageable);
        log.info(
                ">> BranchService :: FindByCriteria :: Criteria[{}] => Result: Total Record[{}] - total pages [{}]",
                (criterionDTO == null ? null : criterionDTO.toString()),
                branches.getTotalElements(),
                branches.getTotalPages());
        // 17/09/21 vanna: chuyển entity sang dto.
        for (Branch branch : branches.getContent()) {
            branchReturns.add(new BranchDTO(branch));
        }
        return new PageImpl<>(branchReturns, pageable, branches.getTotalElements());
    }


}
