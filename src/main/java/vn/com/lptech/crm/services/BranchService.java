package vn.com.lptech.crm.services;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.com.lptech.crm.models.branch.BranchCreateDTO;
import vn.com.lptech.crm.models.branch.BranchDTO;
import vn.com.lptech.crm.models.branch.BranchSearchCriterionDTO;

/**
 * @author: vanna
 * @date: 27/08/2021
 */
public interface BranchService {

  BranchDTO findByCode(String code);

  void create(BranchCreateDTO dto);

  void delete(String code);

  List<BranchDTO> findAll();

  Page<BranchDTO> findByCriteria(BranchSearchCriterionDTO criterionDTO, Pageable pageable)
      throws Exception;

}
