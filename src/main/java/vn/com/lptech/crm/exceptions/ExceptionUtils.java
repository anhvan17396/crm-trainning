package vn.com.lptech.crm.exceptions;

import java.util.HashMap;
import java.util.Map;

public class ExceptionUtils {

  public static final String E_INTERNAL_SERVER = "E_INTERNAL_SERVER";
  public static final String E_COMMON_DUPLICATE_CODE = "E_COMMON_DUPLICATE_CODE";
  public static final String E_COMMON_NOT_EXISTS_CODE = "E_COMMON_NOT_EXISTS_CODE";
  public static final String E_CUSTOMER_DUPLICATE_IDCARD = "E_CUSTOMER_DUPLICATE_IDCARD";
  public static final String E_DIVISION_DUPLICATE_CODE = "E_DIVISION_DUPLICATE_CODE";
  public static final String E_EXPORT_001 = "E_EXPORT_001";
  public static final String E_TITLE_GROUP_IS_ACTIVE = "E_TITLE_GROUP_IS_ACTIVE";
  public static final String E_IMPORT_001 = "E_IMPORT_001";
  public static final String E_IMPORT_002 = "E_IMPORT_002";
  public static final String E_IMPORT_BRANCH_001 = "E_IMPORT_BRANCH_001";
  public static final String E_IMPORT_BRANCH_002 = "E_IMPORT_BRANCH_002";
  public static final String E_IMPORT_CUSTOMER_001 = "E_IMPORT_CUSTOMER_001";
  public static final String E_IMPORT_CUSTOMER_002 = "E_IMPORT_CUSTOMER_002";
  public static final String E_NULL_POINT_RM = "E_NULL_POINT_RM";
  public static final String E_NULL_POINT_DIVISION = "E_NULL_POINT_DIVISION";
  public static final String ERROR_RM_01 = "ERROR_RM_01";
  public static final String ERROR_RM_02 = "ERROR_RM_02";
  public static final String ERROR_RM_03 = "ERROR_RM_03";
  public static final String ERROR_RM_04 = "ERROR_RM_04";
  public static final String ERROR_RM_05 = "ERROR_RM_05";
  public static final String ERROR_RM_06 = "ERROR_RM_06";
  public static final String ERROR_RM_07 = "ERROR_RM_07";
  public static final String ERROR_RM_08 = "ERROR_RM_08";
  public static final String ERROR_RM_09 = "ERROR_RM_09";
  public static final String ERROR_RM_10 = "ERROR_RM_10";
  public static final String ERROR_RM_11 = "ERROR_RM_11";
  public static final String ERROR_RM_12 = "ERROR_RM_12";
  public static final String ERROR_ASSIGNMENT_1 = "ERROR_ASSIGNMENT_1";
  public static final String ERROR_ASSIGNMENT_2 = "ERROR_ASSIGNMENT_2";
  public static final String ERROR_ASSIGNMENT_3 = "ERROR_ASSIGNMENT_3";
  public static final String ERROR_EMPLOYEE_DIVISION_1 = "ERROR_EMPLOYEE_DIVISION_1";
  public static final String ERROR_DIVISION_1 = "ERROR_DIVISION_1";

  public static final String ERROR_TEMPLATE_1 = "ERROR_TEMPLATE_1";
  public static final String ERROR_TEMPLATE_2 = "ERROR_TEMPLATE_2";
  public static final String ERROR_TEMPLATE_3 = "ERROR_TEMPLATE_3";
  public static final String ERROR_TEMPLATE_4 = "ERROR_TEMPLATE_4";
  public static final String ERROR_TEMPLATE_5 = "ERROR_TEMPLATE_5";

  public static final String ERROR_CONFIG_1 = "ERROR_CONFIG_1";
  public static final String ERROR_CONFIG_2 = "ERROR_CONFIG_2";
  public static final String ERROR_CONFIG_3 = "ERROR_CONFIG_3";
  public static final String ERROR_CONFIG_4 = "ERROR_CONFIG_4";


  public static final String RM001 = "RM001";
  public static final String RM002 = "RM002";
  public static final String RM003 = "RM003";
  public static final String RM004 = "RM004";
  public static final String RM005 = "RM005";
  public static final String RM006 = "RM006";
//  public static final String RM007 = "RM007";
//  public static final String RM008 = "RM008";
//  public static final String RM009 = "RM009";
  public static final String RM010 = "RM010";
//  public static final String RM011 = "RM011";



  public static Map<String, String> messages;

  static {
    messages = new HashMap<>();
    messages.put(ExceptionUtils.E_INTERNAL_SERVER, "Server không phản hồi");
    messages.put(ExceptionUtils.E_COMMON_DUPLICATE_CODE, "Code %s đã tồn tại");
    messages.put(ExceptionUtils.E_DIVISION_DUPLICATE_CODE, "Khối %s đã tồn tại");
    messages.put(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE, "Code %s không tồn tại");
    messages.put(ExceptionUtils.E_NULL_POINT_RM, "Không tồn tại RM ứng với tiêu chí tìm kiếm");
    messages.put(ExceptionUtils.E_NULL_POINT_DIVISION, "Không tồn tại khối ứng với tiêu chí tìm kiếm");
    messages.put(ExceptionUtils.E_CUSTOMER_DUPLICATE_IDCARD, "Số chứng minh đã đăng kí");
    messages.put(ExceptionUtils.E_EXPORT_001, "Export excel rỗng");
    messages.put(ExceptionUtils.E_TITLE_GROUP_IS_ACTIVE, "Code %s đang hiệu lực");
    messages.put(ExceptionUtils.E_IMPORT_001, "Tệp tin không đúng định dạng");
    messages.put(ExceptionUtils.E_IMPORT_002, "Tệp tin rỗng");
    messages.put(ExceptionUtils.E_IMPORT_BRANCH_001, "Mã chi nhánh không được để rỗng");
    messages.put(ExceptionUtils.E_IMPORT_BRANCH_002, "Mã chi nhánh đã tồn tại trong hệ thống");
    messages.put(ExceptionUtils.E_IMPORT_CUSTOMER_001, "Mã khách hàng không được để rỗng");
    messages.put(ExceptionUtils.E_IMPORT_CUSTOMER_002, "Mã khách hàng đã tồn tại trong hệ thống");

    messages.put(ExceptionUtils.ERROR_RM_01, "Check trùng trên CRM");
    messages.put(ExceptionUtils.ERROR_RM_02, "Dữ liệu không tồn tại trên hệ thống AD");
    messages.put(ExceptionUtils.ERROR_RM_03, "Core check không có dữ liệu");
    messages.put(ExceptionUtils.ERROR_RM_04, "Core check không có dữ liệu");
    messages.put(ExceptionUtils.ERROR_RM_05, "Core đối chiếu dữ liệu không khớp");
    messages.put(ExceptionUtils.ERROR_RM_06, "Đối chiếu dữ liệu nhập trên CRM với dữ liệu Core");
    messages.put(ExceptionUtils.ERROR_RM_07, "Mã nhân viên không được phép để trống");
    messages.put(ExceptionUtils.ERROR_RM_08, "Thông tin nhân viên đã tồn tại trên hệ thống Core");
    messages.put(ExceptionUtils.ERROR_RM_09, "Email đã đăng kí");
    messages.put(ExceptionUtils.ERROR_RM_10, "Mã RM đã tồn tại");
    messages.put(ExceptionUtils.ERROR_RM_11, "Mã Hris đã tồn tại");
    messages.put(ExceptionUtils.ERROR_RM_12, "Không có thông báo lỗi");

    messages.put(ExceptionUtils.ERROR_ASSIGNMENT_1, "Các KH không thuộc khối RM quản lý");
    messages.put(ExceptionUtils.ERROR_ASSIGNMENT_2, "Các KH đang ở trạng thái chờ phê duyệt");
    messages.put(ExceptionUtils.ERROR_ASSIGNMENT_3, "KH đã được phân giao cho RM");

    messages.put(ExceptionUtils.ERROR_EMPLOYEE_DIVISION_1, "Có bản ghi danh sách khối đã tồn tại");
    messages.put(ExceptionUtils.ERROR_DIVISION_1, "Có khối đã trùng tên");

    messages.put(ExceptionUtils.ERROR_TEMPLATE_1, "Template rỗng");
    messages.put(ExceptionUtils.ERROR_TEMPLATE_2, "TemplateCode hoặc ApplicationCode trống");
    messages.put(ExceptionUtils.ERROR_TEMPLATE_3, "Content rỗng");
    messages.put(ExceptionUtils.ERROR_TEMPLATE_4, "Không tìm thấy Template");
    messages.put(ExceptionUtils.ERROR_TEMPLATE_5, "Template này đã tồn tại");

    messages.put(ExceptionUtils.ERROR_CONFIG_1, "Dữ liệu không tồn tại");
    messages.put(ExceptionUtils.ERROR_CONFIG_2, "Dữ liệu đã tồn tại");
    messages.put(ExceptionUtils.ERROR_CONFIG_3, "Dữ liệu rỗng");
    messages.put(ExceptionUtils.ERROR_CONFIG_4, "TimeNotDisturb và TimeSend không được trống");


    messages.put(ExceptionUtils.RM001, "Bạn phải chọn một RM làm trưởng nhóm");
    messages.put(ExceptionUtils.RM002, "Trưởng nhóm chưa được phân nhóm quản lý");
    messages.put(ExceptionUtils.RM003, "Bạn phải thêm thành viên nhóm");
    messages.put(ExceptionUtils.RM004, "");
    messages.put(ExceptionUtils.RM005, "RM: %s - %s đã là thành viên của nhóm khác");
    messages.put(ExceptionUtils.RM006, "Số lượng RM trong nhóm không được vượt quá số lượng quy định");
//    messages.put(ExceptionUtils.RM007, "");
//    messages.put(ExceptionUtils.RM008, "");
//    messages.put(ExceptionUtils.RM009, "");
    messages.put(ExceptionUtils.RM010, "Trong nhóm chỉ được có 1 nhóm trưởng");
//    messages.put(ExceptionUtils.RM011, "");
  }

  public static String buildMessage(String messKey, Object... arg) {
    return String.format(ExceptionUtils.messages.get(messKey), arg);
  }
}
