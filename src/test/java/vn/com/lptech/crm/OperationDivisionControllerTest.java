package vn.com.lptech.crm;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

public class OperationDivisionControllerTest {
  private MockMvc mockMvc;
  @Autowired private WebApplicationContext webApplicationContext;

  @Autowired private ObjectMapper objectMapper;

  @BeforeEach
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
  }

  @Test
  void findAllShouldReturn200() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/operation/find").queryParam("customerCode", "abc"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void importShouldReturn200() throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders.get("/operation/import"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void exportShouldReturn200() throws Exception {
    mockMvc
        .perform(MockMvcRequestBuilders.get("/operation/exportOperation"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void assignOperationDivisionShouldReturn500() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/operation/")
                .queryParam("requestId", "d42104a9-b6df-455c-80a4-ba756b9ad9de"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void unAssignedOperationShouldReturn500() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/operation/")
                .queryParam("requestId", "d42104a9-b6df-455c-80a4-ba756b9ad9de"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }
}
