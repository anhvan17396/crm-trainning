package vn.com.lptech.crm;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import vn.com.lptech.crm.models.branch.BranchCreateDTO;

@SpringBootTest
public class BranchControllerTests {

  private MockMvc mockMvc;
  @Autowired private WebApplicationContext webApplicationContext;

  @Autowired private ObjectMapper objectMapper;

  @BeforeEach
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
  }

  @Test
  void criteriaShouldReturn200() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/branches/criteria").queryParam("code", "123"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }

  @Test
  void deleteShouldReturn500() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.delete("/branches/1241241243"))
        .andExpect(MockMvcResultMatchers.status().is5xxServerError());
  }

  @Test
  void createShouldReturn500() throws Exception {
    var payload = BranchCreateDTO.builder().code("12344").name("Tran duy hung").build();
    var json = objectMapper.writeValueAsString(payload);
    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/branches")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
        .andExpect(MockMvcResultMatchers.status().is5xxServerError());
  }
}
