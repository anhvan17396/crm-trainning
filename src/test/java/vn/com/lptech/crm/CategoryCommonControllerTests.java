package vn.com.lptech.crm;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@SpringBootTest
public class CategoryCommonControllerTests {
  private MockMvc mockMvc;
  @Autowired private WebApplicationContext webApplicationContext;

  @Autowired private ObjectMapper objectMapper;

  @BeforeEach
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
  }

  @Test
  void criteriaShouldReturn200() throws Exception {

    mockMvc
        .perform(MockMvcRequestBuilders.get("/categoryCommons/criteria").queryParam("code", "aaa"))
        .andExpect(MockMvcResultMatchers.status().isOk());
  }
  @Test
  void findAllCategoryCommonReturn200() throws Exception {

    mockMvc
            .perform(MockMvcRequestBuilders.get("/categoryCommons"))
            .andExpect(MockMvcResultMatchers.status().isOk());
  }
  @Test
  void exportShouldReturn200() throws Exception {
    mockMvc
            .perform(MockMvcRequestBuilders.get("/categoryCommons/export"))
            .andExpect(MockMvcResultMatchers.status().isOk());
  }
}
